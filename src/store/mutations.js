export default {
	rooms(state, value) {
		state.rooms = value
	},
	openPopupPanorama(state, value) {
		state.openPopupPanorama = value
	},
	panoramaRoom(state, value) {
		state.panoramaRoom = value
	},
}
