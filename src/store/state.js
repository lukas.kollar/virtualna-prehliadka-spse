/**
 * 30 - Učebňa počítačových systémov a sietí.
 * 27 - Učebňa robotiky.
 * 87 - IT LAB.
 * 13 - Učebňa internetu vecí.
 * 100 - Učebňa priemyselnej informatiky.
 * 24 - Učebňa slovenského jazyka.
 * 7 - Učebňa cudzích jazykov.
 * 95A - FESTO Učebňa.
 * Telocvičňa - telocvičňa.
 */
export default {
	rooms: [
		{
			name: 'Škola',
			panorama: 'panoramas/spseke.jpg',
		},
		{
			name: 'Učebňa počítačových systémov a sietí',
			panorama: 'panoramas/30.jpg',
		},
		{
			name: 'Učebňa robotiky',
			panorama: 'panoramas/27.jpg',
		},
		{
			name: 'IT LAB',
			panorama: 'panoramas/87.jpg',
		},
		{
			name: 'Učebňa internetu vecí',
			panorama: 'panoramas/13.jpg',
		},
		{
			name: 'Učebňa priemyselnej informatiky',
			panorama: 'panoramas/100.jpg',
		},
		{
			name: 'Učebňa cudzích jazykov',
			panorama: 'panoramas/7.jpg',
		},
		{
			name: 'FESTO Učebňa',
			panorama: 'panoramas/95b.jpg',
		},
		{
			name: 'Telocvičňa',
			panorama: 'panoramas/tv.jpg',
		},
	],
	openPopupPanorama: true,
	panoramaRoom: null,
}
