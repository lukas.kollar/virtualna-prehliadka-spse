import { createApp } from 'vue'
import App from './App.vue'
import store from './store'

const app = createApp(App)

app.use(store)
	.provide('vue', app.config.globalProperties)
	.mount('#app')
